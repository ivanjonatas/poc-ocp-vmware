govc ls -k /Datacenter/host/Cluster

install web server
```bash
$ sudo dnf install httpd -y
```

secret
https://console.redhat.com/openshift/install/pull-secret



### scanning IP in vlan
```bash
$ sudo dnf install nmap -y
```
```bash
$ nmap -sP 192.168.178.0/24
```


### installing cluster
```bash
$ openshift-install create cluster --dir myinst/ --log-level debug > /tmp/log-install.log 2>&1 &
```

### extract binary
```bash
tar xzvf openshift-intall-linux....
```


### base cluster
```bash
$ ssh -i ~/.ssh/c-id-rsa core@10.10.0.9 "sudo oc --kubeconfig=/etc/kubernetes/kubeconfig get pods -A"
```
